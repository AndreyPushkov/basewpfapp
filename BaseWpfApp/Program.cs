﻿using BaseWpfApp.Services.Bg;
using BaseWpfApp.UI.ViewModels;
using BaseWpfApp.UI.Views;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;

namespace BaseWpfApp
{
    class Program
    {
        [STAThread]
        public static void Main()
        {
            var host = Host.CreateDefaultBuilder()
                .ConfigureServices(services =>
                {
                    services.AddSingleton<App>();
                    services.AddSingleton<StatusViewModel>();
                    services.AddSingleton<MainViewModel>();
                    services.AddSingleton<MainWindow>(s => new MainWindow
                    {
                        DataContext = s.GetRequiredService<MainViewModel>()
                        
                    });
                    services.AddSingleton<DataBgService>();
                })
                .Build();
            var app = host.Services.GetService<App>();
            app?.Run();
        }
    }
}
