﻿using System.ComponentModel;


namespace BaseWpfApp.Services.Bg
{
    public abstract class BaseBgService<T, K>
    {
        public delegate void BgVisibilityEvent(bool value);
        public virtual event BgVisibilityEvent WorkerVisibility_Notify;

        public delegate void BgInfoEvent(string message, bool isError = false);
        public virtual event BgInfoEvent WorkerInfo_Notify;

        public delegate void BgStateEvent(int state);
        public virtual event BgStateEvent WorkerState_Notify;

        public delegate void BgCompleteEvent(T result);
        public virtual event BgCompleteEvent WorkerComplete_Notify;

        protected void SendBgVisibilityEvent(bool value) => WorkerVisibility_Notify?.Invoke(value);

        protected void SendBgInfoEvent(string message, bool value = false) => WorkerInfo_Notify?.Invoke(message, value);

        protected void SendBgStateEvent(int value) => WorkerState_Notify?.Invoke(value);

        protected void SendBgCompleteEvent(T value) => WorkerComplete_Notify?.Invoke(value);

        protected static BackgroundWorker worker;

        public abstract void RunWorker(K subj);
        protected abstract void worker_DoWork(object sender, DoWorkEventArgs e);
        protected abstract void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e);

        public virtual void CancelWork()
        {
            if (worker != null)
                worker.CancelAsync();
        }

        protected void CreateWorker()
        {
            if (worker == null)
            {
                worker = new BackgroundWorker();
                worker.WorkerSupportsCancellation = true;
                worker.WorkerReportsProgress = true;
                worker.DoWork += worker_DoWork;
                worker.ProgressChanged += worker_ProgressChanged;
                worker.RunWorkerCompleted += worker_RunWorkerCompleted;
            }
        }

        protected virtual void StartWorker()
        {
            if (worker != null)
            {
                SendBgVisibilityEvent(true);
                worker.RunWorkerAsync();
            }
        }

        protected virtual void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            SendBgInfoEvent((string)e.UserState);
            SendBgStateEvent(e.ProgressPercentage);
        }

        protected virtual void worker_ActionComplete(bool isCanceled, string actionCanceled, string actionComplete)
        {
            if (isCanceled)
                SendBgInfoEvent(actionCanceled);
            else
                SendBgInfoEvent(actionCanceled);
            SendBgStateEvent(0);
            SendBgVisibilityEvent(false);
        }

        protected void CloseWorker()
        {
            if (worker != null)
            {
                worker.DoWork -= worker_DoWork;
                worker.ProgressChanged -= worker_ProgressChanged;
                worker.RunWorkerCompleted -= worker_RunWorkerCompleted;
                worker = null;
            }
        }
    }
}
