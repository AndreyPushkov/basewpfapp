﻿using BaseWpfApp.UI.ViewModels;
using System.ComponentModel;
using System.Threading;

namespace BaseWpfApp.Services.Bg
{
    class DataBgService : BaseBgService<string, string>
    {
        string message;

        // Начальная инициация данных для бизнес логики
        public override void RunWorker(string subj)
        {
            message = subj;
            CreateWorker();
            StartWorker();
        }

        //Выполнение бизнес логики
        protected override void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            foreach (var item in message)
            {
                //SendBgInfoEvent($"process: "); // Почему-то после этой строки не происходит скрытие ПрогрессБар-а
                SendBgStateEvent((message.IndexOf(item) + 1) * 100 / message.Length);
                Thread.Sleep(400);
            }
        }

        // Действия после выполнения бизнес логики
        protected override void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            SendBgVisibilityEvent(false);
            SendBgCompleteEvent($"bg complete: {message}");
            SendBgInfoEvent($"process complete");
            SendBgStateEvent(0);
        }
    }
}
