﻿using System.Reflection;

namespace BaseWpfApp.Helpers
{
    class Constants
    {
        public static string APP_VERSION = Assembly.GetExecutingAssembly().GetName().Version.ToString();
        public static string APP_NAME = "Шаблон";
        public static string MAIN_WINDOW_TITLE = APP_NAME + "     v." + APP_VERSION;
    }
}
