﻿using BaseWpfApp.Helpers;
using BaseWpfApp.Services.Bg;
using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using static BaseWpfApp.Helpers.Constants;


namespace BaseWpfApp.UI.ViewModels
{
    internal class MainViewModel : BaseViewModel, IDisposable
    {
        readonly DataBgService dataService;
        public string MainTitle => MAIN_WINDOW_TITLE;

        public MainViewModel(DataBgService dataService, StatusViewModel status)
        {
            this.dataService = dataService;
            this.dataService.WorkerComplete_Notify += (message) => TestMessage = message;
            Status = status;
            TestCommand = new DelegateCommand(x => dataService.RunWorker("asdf"));
        }

        #region PROPERTIES
        private string testMessage;
        public string TestMessage { get => testMessage ?? "empty message"; set => SetProperty(ref testMessage, value); }

        public StatusViewModel Status { get; private set; }
        #endregion

        #region METHODS
        #endregion

        #region COMMANDS
        public ICommand TestCommand { get; private set; }
        #endregion

        #region COMMANDS

        public ICommand TestCommand { get; private set; }
        #endregion


        public void Dispose()
        {
        }
    }
}
