﻿using BaseWpfApp.Helpers;
using BaseWpfApp.Services.Bg;
using System;
using System.Diagnostics;

namespace BaseWpfApp.UI.ViewModels
{
    class StatusViewModel : BaseViewModel, IDisposable
    {
        readonly DataBgService dataService = new DataBgService();

        public StatusViewModel(DataBgService dataService)
        {
            this.dataService = dataService;
            this.dataService.WorkerInfo_Notify += (message, isError) => { Info = message; IsError = isError; };
            this.dataService.WorkerState_Notify += (state) => State = state;
            this.dataService.WorkerVisibility_Notify += (isVisible) => Visibility = isVisible;
        }

        #region PROPERTIES
        private bool isError = false;
        public bool IsError
        {
            get => isError;
            set => SetProperty(ref visibility, value);
        }

        private bool visibility = false;
        public bool Visibility
        {
            get => visibility;
            set => SetProperty(ref visibility, value);
        }

        private int state = 0;
        public int State
        {
            get => state;
            set => SetProperty(ref state, value);
        }

        private string info = string.Empty;
        public string Info
        {
            get => info ?? "";
            set => SetProperty(ref info, value);
        }
        #endregion

        #region METHODS
        #endregion

        public void Dispose()
        {
        }
    }
}
