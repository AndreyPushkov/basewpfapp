﻿using BaseWpfApp.UI.Views;
using System;
using System.Windows;

namespace BaseWpfApp
{
    internal class App : Application
    {
        readonly MainWindow mainWindow;

        public App(MainWindow mainWindow)
        {
            this.mainWindow = mainWindow;
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            mainWindow.Closed += Window_Closed;
            mainWindow.Show();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            CloseMain();
        }

        public void CloseMain()
        {
            if (mainWindow != null)
            {
                mainWindow.Closed -= Window_Closed;
                mainWindow.Close();
            }
        }
    }
}
